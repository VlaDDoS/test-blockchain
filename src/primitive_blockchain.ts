import * as crypto from 'crypto';

class Block {
  readonly hash: string;

  constructor(
    readonly index: number,
    readonly previousHash: string,
    readonly timestamp: number,
    readonly data: string
  ) {
    this.hash = this.calculateHash();
  }

  /**
   * Создаёт экземпляр объекта Hash, и преобразует хеш-значение в шестнадцатеричную строку
   * @returns string
   */
  private calculateHash(): string {
    const data = this.index + this.previousHash + this.timestamp + this.data;

    return crypto
            .createHash('sha256')
            .update(data)
            .digest('hex');
  }
}

class Blockchain {
  private readonly chain: Block[] = [];

  private get latestBlock(): Block {
    return this.chain[this.chain.length - 1];
  }
  
  constructor() {
    this.chain.push(new Block(0, '0', Date.now(), 'Genesis Block'));
  }

  /**
   * Создает новый экземпляр Block и добавляет его в массив
   * @param data данные blockchain
   */
  public addBlock(data: string): void {
    const block = new Block(
      this.latestBlock.index + 1,
      this.latestBlock.hash,
      Date.now(),
      data
    );

    this.chain.push(block);
  }
}

console.log('Создание блокчейна...');
const blockchain = new Blockchain();

console.log('Блок майнинга #1...');
blockchain.addBlock('Первый блок');

console.log('Блок майнинга #2...');
blockchain.addBlock('Второй блок');

console.log(JSON.stringify(blockchain, null, 2));
